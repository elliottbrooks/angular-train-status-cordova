var app = angular.module('MyApp',['ngRoute','ngCordova']);

var time_rx = /([0-9]*):([0-9]*)/m;

var servername = "http://angular-train-status.herokuapp.com/";

var onDeviceReady = function() {
    angular.bootstrap( document, ['MyApp']);
}
document.addEventListener('deviceready', 
onDeviceReady);

app.config(function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl: 'pages/select.html',
			controller: 'SelectCtrl as select'
		})
		.when('/station/:stationCode',{
			templateUrl: 'pages/times.html',
			controller: 'TrainTimes as times'
		});
})

app.controller('SelectCtrl',['$http','$cordovaGeolocation',function($http,$cordovaGeolocation){
	var state = this;
	this.query = '';
	this.results = null;
	this.locationresults;
	this.loading = false;
	this.locating = false;
	this.crs = null;

	// Attempt to find location
	this.autolocation = function(){
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		state.locating = true;
		// Attempt to get location
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {
			state.locating = false;
			state.loading = true;
			$http.get(servername+'proxy-stationlocation.php?lat='+position.coords.latitude
				+'&long='+position.coords.longitude)
			.success(function(data){
				state.results = data.data;
				state.locationresults = state.results;
				state.loading = false;
			});
		}, function(err) {
			state.locating = false;
		});
	}

	this.autocomplete = function(){
		state.results = null;
		if(state.query==''){
			state.results = state.locationresults;
			return;
		}
		state.loading = true;
		$http.get(servername+'proxy-stationlist.php?q='+state.query).success(function(data){
			state.results = data.data;
			state.loading = false;
		});
	}

	this.selectResult = function(result){
		this.crs = result.CrsCode;
		this.results = null;
		this.query = null;
		window.location = '#station/'+this.crs;
	}

	this.autolocation();
}]);

app.controller('TrainTimes',['$http','$routeParams',function($http,$routeParams){
	var state = this;
	this.stationname = '';
	this.stationcode = $routeParams.stationCode;
	this.loading = false;
	this.trains;
	this.codes;
	this.lastupdated = null;
	this.currenttime = new Date();

	this.setStationCode = function(item){
		this.stationcode = item.code;
		console.log('Code set to '+this.stationcode);
	}

	this.fetchresults = function(){
		state.results = null;
		state.loading = true;
		$http.get(servername+'proxy.php?crs='+state.stationcode).success(function(data){
			state.trains = data[0].Items;
			state.loading = false;
			state.lastupdated = Date.now();
			// Now iterate through results and apply some extra fields
			angular.forEach(state.trains,function(result,key){
				// Is the result cancelled?
				result.Cancelled = (result.ETD=='Cancelled');
				// Is the result delayed
				var etd_rx_results = time_rx.exec(result.ETD);
				if(etd_rx_results){
					// Yes
					result.Delayed = true;
				}else{
					// No
					result.Delayed = false;
				}
			});
		});
	}

	this.calculatestatus = function(result){
		var time_rx_results = time_rx.exec(result.ETD);
		
		// If its not a timestamp, show directly
		if(!time_rx_results){
			return result.ETD;
		}

		// Calculate the minute-stamp of the ETD
		var ETD = time_rx_results[1]*60 + time_rx_results[2]*1;
		// Calculate the minute-stamp of the STD
		var stdtime_rx_results = time_rx.exec(result.STD);
		var STD = stdtime_rx_results[1]*60 + stdtime_rx_results[2]*1;

		// Return the minutes late/early
		if(ETD>STD){
			return (ETD-STD) + ' minutes late';
		}else{
			return (STD-ETD) + ' minutes early';
		}
	}

	this.fetchresults();
}]);